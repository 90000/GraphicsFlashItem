# Qt Graphics View Framework 与 ShockwaveFlash等COM窗体

## ShockwaveFlash的COM窗体

使用dumpcpp导出shockwaveflashobjects.h文件
构造ShockwaveFlashObjects::ShockwaveFlash窗体
加载swf文件
显示窗体后, 可以正常播放Flash

## 使用QGraphicsProxyWidget加载ShockwaveFlashObjects::ShockwaveFlash窗体到QGraphicsScene

创建QGraphicsProxyWidget对象
设置flag
设置ShockwaveFlashObjects::ShockwaveFlash窗体
设置setFocusPolicy
添加到Scene

结果:
1. 可以正常显示Flash界面
1. 鼠标无法操作
1. 设置了可选中, 可移动, 但是无法选中和移动QGraphicsProxyWidget对象

帮助文档中有说明
QGraphicsProxyWidget.setWidget (self, QWidget widget)
Note that widgets with the Qt.WA_PaintOnScreen widget attribute set and widgets that wrap an external application or controller cannot be embedded. Examples are QGLWidget and QAxWidget.

### 使proxyWidget对象可以选中并移动

方法: 阻断event到 widget 的传递
原理: void QGraphicsScenePrivate::mousePressEventHandler(QGraphicsSceneMouseEvent *mouseEvent)中会根据event是否被accepted来判断是否继续进行后续处理
解决实例: 继承 QGraphicsProxyWidget 并重写 mouseEvent 相关函数

class QGraphicsProxyWidgetNoMouseEvent : public QGraphicsProxyWidget
{
	public:

    void mousePressEvent(QGraphicsSceneMouseEvent *event)
    {
        QGraphicsItem::mousePressEvent(event);
    }
    void mouseMoveEvent(QGraphicsSceneMouseEvent *event)
    {
        QGraphicsItem::mouseMoveEvent(event);
    }
    void mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
    {
        QGraphicsItem::mouseReleaseEvent(event);
    }
};

### 通过某种方式可以触发 ShockwaveFlashObjects::ShockwaveFlash 窗体的显示

方法:
1. 从 QGraphicsProxyWidget 对象中移除 COM 窗体的显示
1. 隐藏 COM 窗体
1. 显示 COM 窗体

原理:
1. 去除 COM 窗体被代理的状态
1. 清楚 COM 窗体的显示状态
1. 正常显示 COM 窗体

实例:
    flashItem->setWidget(0);
    comflash->hide();
    comflash->show();
	
### 引申

1. QGraphicsProxyWidgetNoMouseEvent 中添加paint方法, 并使用 QPixmap 保存 COM 窗体的预览图
1. 通过某种方式触发窗体的显示, QGraphicsProxyWidgetNoMouseEvent中改为显示预览图
1. 正常操作弹出窗体中的Flash
1. 感知弹出窗体关闭动作
1. 重新添加 COM 窗体为代理窗体
1. QGraphicsProxyWidgetNoMouseEvent 改为显示代理窗体
