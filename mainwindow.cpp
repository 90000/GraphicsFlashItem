#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "shockwaveflashobjects.h"
#include <QFileDialog>
#include <QGraphicsProxyWidget>
#include <QTimer>
#include <QtWebEngineWidgets/QWebEngineView>
#include <QWebEngineSettings>
#include "dump.h"

class QGraphicsProxyWidgetNoMouseEvent : public QGraphicsProxyWidget
{
public:
    void mousePressEvent(QGraphicsSceneMouseEvent *event)
    {
        QGraphicsItem::mousePressEvent(event);
    }
    void mouseMoveEvent(QGraphicsSceneMouseEvent *event)
    {
        QGraphicsItem::mouseMoveEvent(event);
    }
    void mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
    {
        QGraphicsItem::mouseReleaseEvent(event);
    }
};

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    scene(new QGraphicsScene(this))
{
    ui->setupUi(this);
    ui->mainToolBar->addAction("InsertFlash", this, SLOT(insertFlash()));
    ui->mainToolBar->addAction("InsertFlashWeb", this, SLOT(insertFlashWeb()));
    ui->centralWidget->setScene(scene);

    QWebEngineSettings::defaultSettings()->setAttribute(QWebEngineSettings::PluginsEnabled, true);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::insertFlash()
{
    QString path = QFileDialog::getOpenFileName(this, QString(), QString(), "*.swf");
    if(path.isEmpty())
        return;

    ShockwaveFlashObjects::ShockwaveFlash *comflash = new ShockwaveFlashObjects::ShockwaveFlash;
    QGraphicsProxyWidget *flashItem = new QGraphicsProxyWidgetNoMouseEvent;
    flashItem->setFlag(QGraphicsItem::ItemIsMovable);
    flashItem->setFlag(QGraphicsItem::ItemIsSelectable);
    flashItem->setFlag(QGraphicsItem::ItemIsFocusable);
    flashItem->setWidget(comflash);
    flashItem->setGeometry(QRectF(0,0,500,400));
    flashItem->setFocusPolicy(Qt::StrongFocus);

    comflash->LoadMovie(0, path);

    scene->addItem(flashItem);
    flashItem->setSelected(true);

//    flashItem->setWidget(0);
//    comflash->hide();
    //    comflash->show();
}

void MainWindow::insertFlashWeb()
{
    QString path = QFileDialog::getOpenFileName(this, QString(), QString(), "*.swf");
    if(path.isEmpty())
        return;

    SWFDumper dumper;
    dumper.openSWF(path);
    QSize size = dumper.getSWFSize();

    QWebEngineView *view = new QWebEngineView;
    QString html = "<!DOCTYPE html> <html lang=""> <head> <title>Test</title> </head> <body> <p><object type='application/x-shockwave-flash' data='%1' width=%2 height=%3> <param name='movie' value='%1' /> </object></p> </body> </html>";
    html = html.arg(path).arg(size.width()).arg(size.height());
    qDebug()<<html;
    view->page()->setHtml(html);
//    view->show();

    QGraphicsProxyWidget *flashItem = new QGraphicsProxyWidget;
    flashItem->setFlag(QGraphicsItem::ItemIsMovable);
    flashItem->setFlag(QGraphicsItem::ItemIsSelectable);
    flashItem->setFlag(QGraphicsItem::ItemIsFocusable);
    flashItem->setWidget(view);
//    flashItem->setGeometry(QRectF(0,0,size.width(),size.height()));
    flashItem->setFocusPolicy(Qt::StrongFocus);
    scene->addItem(flashItem);
    flashItem->setSelected(true);
}
